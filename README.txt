Description
-----------
This module adds a new type of Webform component: structured / masked-input text. Specify an input mask, and the form input will be rendered to accept that mask.

How to use
----------
- Go to the main field list of a webform.
- Add a structured text element to your form.
- In the component settings, specify an input mask, and optional labels, placeholders, RegEx validations, and default values.
- Save the component.
- The component will be rendered as distinct text fields and markup per your mask.

Theming
-------
The default behaviour for this component is to render all individual input boxes inside a <div> with a border, and no borders around the individual input boxes.  This is designed to mimic a standard text input field, but the look may differ from the rest of your input fields, depending on how your theme renders them.  Review the file webform_structured_text.css to determine what classes you should override in your theme.